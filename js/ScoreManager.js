let abi = [
  {
    inputs: [
      {
        internalType: "uint256",
        name: "_scores",
        type: "uint256",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "addScore",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "uint256",
        name: "_scores",
        type: "uint256",
      },
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "deductScore",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "resetScore",
    outputs: [],
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    inputs: [
      {
        internalType: "address",
        name: "account",
        type: "address",
      },
    ],
    name: "getScore",
    outputs: [
      {
        internalType: "uint256",
        name: "",
        type: "uint256",
      },
    ],
    stateMutability: "view",
    type: "function",
  },
];

let account = "0x8A9a4147A0c3c4ff45c2DC14a3aC94D1c899E0Cf";
let privateKey = process.env.PRIVATE_KEY;
let rpcL1 = "https://rpc-testnet.bitkubchain.io";
let rpcL2 = "https://testnet-rpc.bkcping.com";
const provider = new ethers.providers.JsonRpcProvider(rpcL2);
const signer = new ethers.Wallet(privateKey, provider);
const contract = new ethers.Contract(
  "0xF0f3Db9a1C39c580e966Ea0D21F3c0395824a87f",
  abi,
  signer
);

let nonce = 0;
async function addScore(score) {
  return mutex
    .promise()
    .then(function (mutex) {
      mutex.lock();
      let tx = contract.addScore(score.toString(), account, {
		nonce: nonce++,
	  });
	  return tx;
    })
    .then(function (res) {
      mutex.unlock();
    //   console.log(res);
      return res;
    })
    .catch(function (e) {
      mutex.unlock();
      throw e;
    });
//   console.log("Nonce : ", nonce);
  
}

async function getScore() {
  let tx = await contract.getScore(account);
  console.log("Score : ", tx.toString());
  return tx.toString();
}

async function deductScore(score) {
  let tx = await contract.deductScore(score.toString(), account);
  await getScore();
  return tx;
}

window.onload = async function () {
  await contract.resetScore(account);
  nonce = await provider.getTransactionCount(account);
};
