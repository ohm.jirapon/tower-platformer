const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Score", function () {
  it("Should Success!!!!!!!!!!!!!", async function () {
    const [acc1] = await ethers.getSigners();
    const ScoreManager = await ethers.getContractFactory("ScoreManager");
    const scoreManager = await ScoreManager.deploy();
    await scoreManager.deployed();

    expect(await scoreManager.getScore(acc1.address)).to.equal("0");

    await scoreManager.updateScore("10", acc1.address);

    // // wait until the transaction is mined
    // await setGreetingTx.wait();

    expect(await scoreManager.getScore(acc1.address)).to.equal("10");
  });
});
