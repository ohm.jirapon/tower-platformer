pragma solidity 0.8.4;

contract ScoreManager {

    mapping(address => uint256) scores;

    function getScore(address account) external view returns(uint256) {
        return scores[account];
    }
    function addScore(uint256 _scores, address account) external {
        scores[account] +=  _scores;
    }

    function deductScore(uint256 _scores, address account) external {
        if (_scores > scores[account]) {
            scores[account] = 0;
        }
        scores[account] -=  _scores;
    }

    function resetScore(address account) external {
        scores[account] = 0;
    }
}